<?php
function dot($matrixA, $matrixB){
    $res = 0;
    if(!empty($matrixA)){
	    for($i=0; $i < count($matrixA); ++$i) {
	    	if(empty($matrixA[$i]) || empty($matrixB[$i])){
	    		$res += 0;
	    		continue;
	    	}
	        $res += $matrixA[$i] * $matrixB[$i];
	    }
    }
    return $res;
}

function showTable($matrix){
    foreach ($matrix as $key=>$value){
        foreach ($value as $k=>$v){
            echo $v.'  ';
        }
        echo "<br />";
    }
    echo "<br /><br />";
}
?>

<?php
$startTime = round(microtime(true),4);

$csvFile = file('files/ratings.csv');

$numF = 2;//кол-во признаков

$mu = 0;
$step = 0;
$rmse = 1;
$rmseOld = 0;
$eta = 0.1;//скорость обучения
$speeControl = 0.01;

$total = 0;
$maxV = 0;
$maxU = 0;

//получение рейтингов из файла
foreach ($csvFile as $key => $line) {
    $a = str_getcsv($line, ',');
    $matrix[$a[0]][$a[1]] = $a[2];

    if ($a[1] > $maxV) { $maxV = $a[1]; }
    if ($a[0] > $maxU) { $maxU = $a[0]; }
    ++$total;
}

$lambda = 0.5/$maxV;//параметр регуляризации

//предикторы
for($i=0; $i < $maxU; $i++){
    $bu[$i] = 0.0;
}
for($i=0; $i < $maxV; $i++){
    $bv[$i] = 0.0;
}

//признаки
$uf = [];
$vf = [];
for ($u=0; $u < $maxU; ++$u) {
    for ($f=0; $f < $numF; ++$f) {
        $uf[$u][$f] = 0.1;
    }
}
for ($v=0; $v<$maxV; ++$v) {
    for ($f = 0; $f < $numF; ++$f) {
        $vf[$v][$f] = 0.05 * $f;
    }
}

//обучение SVD
$startTimeSvd = round(microtime(true),4);
while (abs($rmseOld - $rmse) > 0.00001 ) {
    $rmseOld = $rmse;
    $rmse = 0;
    foreach($matrix as $u => $val){
        foreach ($val as $v => $rating){
            //ошибка
            $err = $matrix[$u][$v] - ($mu + $bu[$u] + $bv[$v] + dot($uf[$u] , $vf[$v]));
            //квадрат ошибки
            $rmse += $err * $err;
            
            //обновление базовых предикторов
            $mu += $eta * $err;
            $bu[$u] += $eta * ($err - $lambda * $bu[$u]);
            $bv[$v] += $eta * ($err - $lambda * $bv[$v]);
            //и векторов признаков
            for ($f=0; $f < $numF; ++$f) {
                $uf[$u][$f] += $eta * ($err * $vf[$v][$f] - $lambda * $uf[$u][$f]);
                $vf[$v][$f] += $eta * ($err * $uf[$u][$f] - $lambda * $vf[$v][$f]);
            }
        }
    }

    ++$step;
    //нормирование суммарной ошибки, чтобы получить RMSE
    $rmse = sqrt($rmse / $total);
    echo "Iteration $step: RMSE=$rmse <br />";

    //если RMSE меняется мало, уменьшается скорость обучения
    if ($rmse > $rmseOld - $speeControl) {
        $eta = $eta * 0.66;
        $speeControl = $speeControl * 0.5;
    }
}
$endTimeSvd = round(microtime(true),4);

//получение ТОП рекомендаций
$user = 612;//номер пользователя
$maxTop = 5;//кол-во рекомендаций
$rn = array();
$top = array();
for($i=0; $i < $maxV; $i++){
    $r = $mu + $bu[$user] + $bv[$i] + dot($uf[$user] , $vf[$i]);

    if($i < $maxTop){
        $rn[$i] = round($r, 4);
        $top[$i] = $i;
    }
    else{
        $min = min($rn);
        if($min < $r){
            $minKey = array_keys($rn, $min);
            $rn[$minKey[0]] = $r;
            $top[$minKey[0]] = $i;
        }
    }
}

//вывод ТОП
$csvFile2 = file('files/movies.csv');
foreach ($csvFile2 as $key=>$line) {
    $a = str_getcsv($line, ',');
    $films[$a[0]] = $a[1];
}

echo "<br /> ******** ТОП ******** <br />";
foreach ($top as $filmID){
    echo $films[$filmID]."<br />";
}

$endTime = round(microtime(true),4);

echo "<br /> ******** ВРЕМЯ ******** <br />";
echo "Работа SVD: ".substr(($endTimeSvd - $startTimeSvd),0,5);
echo "<br />";
echo "Выполнение кода: ".substr(($endTime - $startTime),0,5);
?>